#include <iostream>
#include <math.h>

double getAbsoluteInaccuracy(double x, double xStar) {
    return fabs(x - xStar); //#1
}

double getRelativeInaccuracy(double x, double xStar) {
    return getAbsoluteInaccuracy(x, xStar) / fabs(xStar); // #2
}

//9 вариант
int main() {
    double x = 6.0/11.0;
    double xStar = 0.545;

    double y = sqrt(83);
    double yStar = 9.11;

    double xInaccuracy = getRelativeInaccuracy(x, xStar);
    double yInaccuracy = getRelativeInaccuracy(y, yStar);

    if(xInaccuracy == yInaccuracy) {
        std:: cout << "Inaccuracies are equals" << std::endl;
        return 0;
    }

    std::cout << (xInaccuracy > yInaccuracy ? "Second" : "First");
    std::cout << " statement are more accurate" << std::endl;

    return 0;
}
