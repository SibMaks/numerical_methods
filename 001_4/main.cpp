#include <iostream>
#include <math.h>
#include <iomanip>

#define LLI long long int

LLI getNumberCount(double x) {
    std::stringstream ss;
    ss << std::fixed << std::setprecision(15) <<  x;
    std::string strNum = ss.str();
    size_t pos = strNum.find('.');
    return strNum.size() - (pos == strNum.size() ? 0 : 1);
}

LLI getFirstZeros(double x) {
    std::stringstream ss;
    ss << std::fixed << std::setprecision(15) <<  x;
    std::string strNum = ss.str();
    LLI zeros = 0;
    for(auto c : strNum) {
        if(c == '0') {
            zeros++;
        } else if(c != '.') {
            break;
        }
    }
    return zeros;
}

LLI getNumberCountAfterDot(double x) {
    std::stringstream ss;
    ss << std::fixed << std::setprecision(15) <<  x;
    std::string strNum = ss.str();
    size_t pos = strNum.find('.');
    return (pos == strNum.size() ? 0 : strNum.size() - 1 - pos);
}

LLI getValidSignificantNumbersCount(double x, double xAI) {
    LLI ncad = getNumberCountAfterDot(x);
    LLI nc = getNumberCount(x);
    LLI zeros = getFirstZeros(x);

    LLI s = -1;

    //#1
    double now = 0.5 * pow(10, -ncad);
    for(LLI i = 0; i < ncad; i++) {
        if(now > xAI) {
            s = nc - i;
            break;
        }
        now *= 10;
    }

    if(s == -1) {
        for(LLI i = 0; i < nc - ncad; i++) {
            if(now > xAI) {
                s = nc - ncad - i;
                break;
            }
            now *= 10;
        }
    }

    return s - zeros;
}

//#2
double getAIFromRI(double x, double xRI) {
    return fabs(x) * xRI;
}

//#3
double getRIFromAI(double x, double xAI) {
    return xAI / fabs(x);
}

int main() {
    std::cout << std::fixed << std::setprecision(10);

    double aStar = 0.123456;
    double aAI = 0.0005;
    double aRI = getRIFromAI(aStar, aAI);
    std::cout << "A: " << aStar << ", " << aRI << ", " << aAI << std::endl;

    double bStar = 0.0078;
    double bAI = 0.00003;
    double bRI = getRIFromAI(bStar, bAI);
    std::cout << "B: " << bStar << ", " << bRI << ", " << bAI << std::endl;

    double cStar = 0.008;
    double cAI = 0.00013;
    double cRI = getRIFromAI(cStar, cAI);
    std::cout << "C: " << cStar << ", " << cRI << ", " << cAI << std::endl;

    //first
    double firstValue = pow(1 + cStar, 1.0/3.0);
    double firstAI = fabs(1.0/3.0 * pow((1 + cStar), -2.0/3.0) * cAI); //#4
    double firstRI = getRIFromAI(firstValue, firstAI);
    std::cout << "1: " << firstValue << ", " << firstRI << ", " << firstAI << std::endl;
    //second
    double secondValue = aStar * bStar;
    double sRI = aRI + bRI; //#5
    double sAI = getAIFromRI(secondValue, sRI);
    std::cout << "2: " << secondValue << ", " << sRI << ", " << sAI << std::endl;
    //third
    double thirdValue = secondValue / firstValue;
    double thirdRI = firstRI + sRI; //#5
    double thirdAI = getAIFromRI(thirdValue, thirdRI);
    std::cout << "3: " << thirdValue << ", " << thirdRI << ", " << thirdAI << std::endl;
    //fourth
    double fourValue = aStar + bStar;
    double fourAI = aAI + bAI; //#6
    double fourRI = getRIFromAI(fourValue, fourAI);
    std::cout << "4: " << fourValue << ", " << fourRI << ", " << fourAI << std::endl;
    //fifth
    double fifthValue = sin(cStar);
    double fifthAI = fabs(cos(cStar)) * cAI;
    double fifthRI = getRIFromAI(fifthValue, fifthAI);
    std::cout << "5: " << fifthValue << ", " << fifthRI << ", " << fifthAI << std::endl;
    //sixth
    double sixValue = thirdValue * fourValue;
    double sixRI = thirdRI + fourRI;
    double sixAI = getAIFromRI(sixValue, sixRI);
    std::cout << "6: " << sixValue << ", " << sixRI << ", " << sixAI << std::endl;
    //final
    double fValue = sixValue * fifthValue;
    double fRI = sixRI + fifthRI;
    double fAI = getAIFromRI(fValue, fRI);
    std::cout << "F: " << fValue << ", " << fRI << ", " << fAI << std::endl;

    std::cout << "Significant numbers: " << getValidSignificantNumbersCount(fValue, fAI) << std::endl;

    return 0;
}
