#include <iostream>
#include <math.h>
#include <iomanip>

#define LLI long long int

LLI getNumberCountAfterDot(double x) {
    std::stringstream ss;
    ss << std::setprecision(15) <<  x;
    std::string strNum = ss.str();
    size_t pos = strNum.find('.');
    return (pos == strNum.size() ? 0 : strNum.size() - 1 - pos);
}

double getAbsoluteInaccuracy(double x) {
    LLI ncad = getNumberCountAfterDot(x);
    if(ncad == 0) {
        return 0.5;
    }
    return 0.5 * pow(10, -ncad);
}

int main() {
    double x = 47.72;
    double aX = getAbsoluteInaccuracy(x);
    double rX = aX * fabs(x);

    std::cout << aX << std::endl;
    std::cout << rX << std::endl;
    return 0;
}
