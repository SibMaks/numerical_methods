#include <iostream>
#include <math.h>
#include <iomanip>

#define LLI long long int

LLI getNumberCount(double x) {
    std::stringstream ss;
    ss << std::fixed << std::setprecision(15) <<  x;
    std::string strNum = ss.str();
    size_t pos = strNum.find('.');
    return strNum.size() - (pos == strNum.size() ? 0 : 1);
}

LLI getFirstZeros(double x) {
    std::stringstream ss;
    ss << std::fixed << std::setprecision(15) <<  x;
    std::string strNum = ss.str();
    LLI zeros = 0;
    for(auto c : strNum) {
        if(c == '0') {
            zeros++;
        } else if(c != '.') {
            break;
        }
    }
    return zeros;
}

LLI getNumberCountAfterDot(double x) {
    std::stringstream ss;
    ss << std::fixed << std::setprecision(15) <<  x;
    std::string strNum = ss.str();
    size_t pos = strNum.find('.');
    return (pos == strNum.size() ? 0 : strNum.size() - 1 - pos);
}

LLI getValidSignificantNumbersCount(double x, double xAI) {
    LLI ncad = getNumberCountAfterDot(x);
    LLI nc = getNumberCount(x);
    LLI zeros = getFirstZeros(x);

    LLI s = -1;

    //#1
    double now = 0.5 * pow(10, -ncad);
    for(LLI i = 0; i < ncad; i++) {
        if(now > xAI) {
            s = nc - i;
            break;
        }
        now *= 10;
    }

    if(s == -1) {
        for(LLI i = 0; i < nc - ncad; i++) {
            if(now > xAI) {
                s = nc - ncad - i;
                break;
            }
            now *= 10;
        }
    }

    return s - zeros;
}

double getAI(double x, int m) {
    LLI zeros = getFirstZeros(x);
    LLI ncad = getNumberCountAfterDot(x);
    LLI n = getNumberCount(x);
    LLI before = n - ncad;
    if(zeros == 0) {
        return 0.5 * pow(10, before - m);
    }
    return 0.5 * pow(10, -(zeros - 1 + m));
}

//#2
double getAIFromRI(double x, double xRI) {
    return fabs(x) * xRI;
}

//#3
double getRIFromAI(double x, double xAI) {
    return xAI / fabs(x);
}

int main() {
    std::cout << std::fixed << std::setprecision(10);

    double aStar = 0.02456;
    double bStar = 0.007823;
    double cStar = 0.8348;
    int m = 4;

    double f = (aStar + bStar) / (aStar - bStar) * atan(aStar * cStar);
    double fAI = getAI(f, m);

    double fDa = (cStar * (aStar * aStar - bStar * bStar) - 2 * (pow(aStar * cStar, 2) * bStar + bStar) * atan(aStar * cStar))
            / (pow((aStar - bStar), 2) * (pow(aStar * cStar, 2) + 1));
    double aAI = fAI / (3.0 * fabs(fDa));

    double fDb = (2.0 * aStar * atan(aStar * cStar)) / pow(aStar - bStar, 2);
    double bAI = fAI / (3.0 * fabs(fDb));

    double fDc = (aStar * (aStar + bStar)) / ((aStar - bStar) * (pow(aStar * cStar, 2) + 1));
    double cAI = fAI / (3.0 * fabs(fDc));

    std::cout << f << ", " << fAI << std::endl;
    std::cout << "a ai: " << aAI << std::endl;
    std::cout << "b ai: " << bAI << std::endl;
    std::cout << "c ai: " << cAI << std::endl;

    return 0;
}
