#include <iostream>
#include <math.h>
#include <iomanip>

#define LLI long long int

LLI getNumberCount(double x) {
    std::stringstream ss;
    ss << std::fixed << std::setprecision(15) <<  x;
    std::string strNum = ss.str();
    size_t pos = strNum.find('.');
    return strNum.size() - (pos == strNum.size() ? 0 : 1);
}

LLI getNumberCountAfterDot(double x) {
    std::stringstream ss;
    ss << std::fixed << std::setprecision(15) <<  x;
    std::string strNum = ss.str();
    size_t pos = strNum.find('.');
    return (pos == strNum.size() ? 0 : strNum.size() - 1 - pos);
}

LLI getValidSignificantNumbersCount(double x, double aX) {
    LLI ncad = getNumberCountAfterDot(x);
    LLI nc = getNumberCount(x);

    LLI s = -1;

    //#1
    double now = 0.5 * pow(10, -ncad);
    for(LLI i = 0; i < ncad; i++) {
        if(now > aX) {
            s = nc - i;
            break;
        }
        now *= 10;
    }

    if(s == -1) {
        for(LLI i = 0; i < nc - ncad; i++) {
            if(now > aX) {
                s = nc - ncad - i;
                break;
            }
            now *= 10;
        }
    }

    return s;
}

//9 вариант
int main() {
    double x = 21.68563;
    double aX = 0.3 / 100.0 * x; //#2
    double y = 21.68563;
    double aY = 0.0041;

    std::cout << getValidSignificantNumbersCount(x, aX) << std::endl;
    std::cout << getValidSignificantNumbersCount(y, aY) << std::endl;

    return 0;
}
